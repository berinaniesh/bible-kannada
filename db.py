import psycopg
import requests
import glob
import subprocess

API = "https://api.bible.berinaniesh.xyz"
count = 0
usfms = glob.glob("usfm/*")
abbreviations = requests.get(f"{API}/abbreviations").json()

book_names = [
    "ಆದಿಕಾಂಡ",
    "ವಿಮೋಚನಕಾಂಡ",
    "ಯಾಜಕಕಾಂಡ",
    "ಅರಣ್ಯಕಾಂಡ",
    "ಧರ್ಮೋಪದೇಶಕಾಂಡ",
    "ಯೆಹೋಶುವ",
    "ನ್ಯಾಯಸ್ಥಾಪಕರು",
    "ರೂತಳು",
    "1 ಸಮುವೇಲನು",
    "2 ಸಮುವೇಲನು",
    "1 ಅರಸುಗಳು",
    "2 ಅರಸುಗಳು",
    "1 ಪೂರ್ವಕಾಲವೃತ್",
    "2 ಪೂರ್ವಕಾಲವೃತ್",
    "ಎಜ್ರನು",
    "ನೆಹೆಮಿಯ",
    "ಎಸ್ತೇರಳು",
    "ಯೋಬನು",
    "ಕೀರ್ತನೆಗಳು",
    "ಙ್ಞಾನೋಕ್ತಿಗಳು",
    "ಪ್ರಸಂಗಿ",
    "ಪರಮ ಗೀತ",
    "ಯೆಶಾಯ",
    "ಯೆರೆಮಿಯ",
    "ಪ್ರಲಾಪಗಳು",
    "ಯೆಹೆಜ್ಕೇಲನು",
    "ದಾನಿಯೇಲನು",
    "ಹೋಶೇ",
    "ಯೋವೇಲ",
    "ಆಮೋಸ",
    "ಓಬದ್",
    "ಯೋನ",
    "ಮಿಕ",
    "ನಹೂಮ",
    "ಹಬಕ್ಕೂಕ್",
    "ಚೆಫನ್ಯ",
    "ಹಗ್ಗಾಯ",
    "ಜೆಕರ್ಯ",
    "ಮಲಾಕಿಯ",
    "ಮತ್ತಾಯನು",
    "ಮಾರ್ಕನು",
    "ಲೂಕನು",
    "ಯೋಹಾನನು",
    "ಅಪೊಸ್ತಲರ ಕೃತ್ಯಗ",
    "ರೋಮಾಪುರದವರಿಗೆ",
    "1 ಕೊರಿಂಥದವರಿಗೆ ं",
    "2 ಕೊರಿಂಥದವರಿಗೆ",
    "ಗಲಾತ್ಯದವರಿಗೆ",
    "ಎಫೆಸದವರಿಗೆ",
    "ಫಿಲಿಪ್ಪಿಯವರಿಗೆ",
    "ಕೊಲೊಸ್ಸೆಯವರಿಗೆ",
    "1 ಥೆಸಲೊನೀಕದವರಿಗೆ",
    "2 ಥೆಸಲೊನೀಕದವರಿಗೆ",
    "1 ತಿಮೊಥೆಯನಿಗೆ",
    "2 ತಿಮೊಥೆಯನಿಗೆ",
    "ತೀತನಿಗೆ",
    "ಫಿಲೆಮೋನನಿಗೆ",
    "ಇಬ್ರಿಯರಿಗೆ",
    "ಯಾಕೋಬನು",
    "1 ಪೇತ್ರನು",
    "2 ಪೇತ್ರನು",
    "1 ಯೋಹಾನನು",
    "2 ಯೋಹಾನನು",
    "3 ಯೋಹಾನನು",
    "ಯೂದನು",
    "ಪ್ರಕಟನೆ",
]

connection = psycopg.connect(dbname="biblenew")
cursor = connection.cursor()


def reset_db():
    global connection
    global cursor
    connection.close()
    subprocess.run(["./reset_db.sh"], shell=True, capture_output=True)
    connection = psycopg.connect(dbname="biblenew")
    cursor = connection.cursor()


def get_book_name(abbreviation):
    ab_index = 0
    global book_names
    for i in range(len(abbreviations)):
        if abbreviation == abbreviations[i]:
            ab_index = i
            break
    return book_names[i]


def get_fname(abbreviation):
    global usfms
    for usfm in usfms:
        if abbreviation in usfm.upper():
            return usfm
    raise Exception(f"Book not found for abbreviation {abbreviation}")


def push_mlsvp():
    global cursor
    global connection
    lang_id = cursor.execute(
        """
                             INSERT INTO "Language" ("name") VALUES ('Kannada') RETURNING "id"
                             """
    ).fetchone()[0]
    translation_id = cursor.execute(
        """
                                    INSERT INTO "Translation" ("language_id", "name", "full_name", "bible_name", "year", "license", "description") VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING "id"
                                    """,
        (
            lang_id,
            "KOVBSI",
            "Kannada Old Version Bible Society of India",
            "ಪವಿತ್ರ ಬೈಬಲ್ ",
            "1951",
            "Public Domain",
            'Published by "The Bible Society of India".\nDigitization done by The Free Bible Foundation (TFBF) volunteers.\nGitHub source can be found at "https://github.com/tfbf/Bible-Kannada-1951".\n',
        ),
    ).fetchone()[0]
    cursor.execute(
        """
                   INSERT INTO "TestamentName" ("translation_id", "testament", "name") VALUES (%s, %s, %s)
                   """,
        (translation_id, "OldTestament", "ಹಳೆಯ ಒಡಂಬಡಿಕೆ"),
    )
    cursor.execute(
        """
                   INSERT INTO "TestamentName" ("translation_id", "testament", "name") VALUES (%s, %s, %s)
                   """,
        (translation_id, "NewTestament", "ಹೊಸ ಒಡಂಬಡಿಕೆಯು"),
    )
    connection.commit()
    return translation_id


def process_ab(ab, translation_id):
    global cursor
    global connection
    book_id = cursor.execute(
        """
                             SELECT id FROM "Book" WHERE abbreviation=%s
                             """,
        (ab,),
    ).fetchone()[0]
    fname = get_fname(ab)
    name_pushed = False
    book_verse_count = 0
    with open(fname, "r") as f:
        usfm_content = f.read()
    usfm_contents = usfm_content.split("\n")
    current_chapter = 0
    current_chapter_id = 0
    current_verse = ""
    long_title = ""
    short_title = get_book_name(ab)
    cursor.execute(
        """
                    INSERT INTO "TranslationBookName" ("translation_id", "book_id", "name") VALUES (%s, %s, %s)
                    """,
        (translation_id, book_id, short_title),
    )
    for c in usfm_contents:
        skip = False
        if c.startswith("\\c"):
            lll = c.split(" ")
            try:
                current_chapter = int(lll[1])
            except:
                print(ab)
                print(c)
                raise Exception("sdf")
            current_chapter_id = int(
                cursor.execute(
                    """
                                                SELECT id FROM "Chapter" WHERE book_id=%s and chapter_number=%s
                                                """,
                    (book_id, current_chapter),
                ).fetchone()[0]
            )
        elif c.startswith("\\v"):
            try:
                verse_number = int(c.split(" ")[1])
            except:
                global count
                print(ab)
                print(current_chapter)
                print(c)
                count +=1
            verse_text = " ".join(c.split(" ")[2:])
            verse_id = 0
            skip = True
            try:
                if not skip:
                    verse_id = cursor.execute(
                        """
                                            SELECT id from "Verse" WHERE chapter_id=%s and verse_number=%s
                                            """,
                        (current_chapter_id, verse_number),
                    ).fetchone()[0]
            except TypeError:
                if not skip:
                    print("Came to exception")
                    verse_id = cursor.execute(
                        """
                                            INSERT INTO "Verse" ("chapter_id", "verse_number") VALUES (%s, %s) RETURNING id
                                            """,
                        (current_chapter_id, verse_number),
                    ).fetchone()[0]
                cursor.execute(
                    """
                            INSERT INTO "VerseText" ("translation_id", "verse_id", "verse") VALUES (%s, %s, %s)
                            """,
                    (translation_id, verse_id, verse_text),
                )
    connection.commit()


reset_db()
translation_id = push_mlsvp()
for abbreviation in abbreviations:
    process_ab(abbreviation, translation_id)

connection.close()
